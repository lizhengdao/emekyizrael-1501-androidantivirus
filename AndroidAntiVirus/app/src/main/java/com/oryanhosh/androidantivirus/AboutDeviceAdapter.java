package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

class AboutDeviceAdapter extends ArrayAdapter<String> {
    private Context context;
    private int resource;
    private String[] details;

    public AboutDeviceAdapter(@NonNull Context context, int resource, @NonNull String[] details) {
        super(context, resource, details);
        this.context = context;
        this.resource = resource;
        this.details = details;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);
        TextView textViewTitle = view.findViewById(R.id.textViewDetailTitle);
        TextView textViewContent = view.findViewById(R.id.textViewDetailContent);
        textViewTitle.setText(details[position].split(":")[0]);
        textViewContent.setText(details[position].split(":")[1]);
        return view;
    }
}
