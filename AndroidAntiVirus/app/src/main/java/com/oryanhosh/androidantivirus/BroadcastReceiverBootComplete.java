package com.oryanhosh.androidantivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class BroadcastReceiverBootComplete extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //check if BOOT_COMPLETED
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()))
        {
            Log.i(Constants.DEBUG_TAG, "BOOT_COMPLETED");
            Intent serviceIntent = new Intent(context, MyService.class);
            serviceIntent.putExtra("inputExtra", Constants.TYPE_INIT_MESSAGE + "nothing");
            ContextCompat.startForegroundService(context, serviceIntent); //start service
        }
    }
}
