package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * This class handles the creation of the TCPClient and registers for progress (new data) from the server
 * Created by miles on 12/16/15.
 */
class ConnectionManagerTask extends AsyncTask<String, String, TCPClient> {

    private Context context;
    public TCPClient mTcpClient;

    public ConnectionManagerTask(Context context) {
        this.context = context;
    }

    /**
     * This function is basically an initializer and creates the TCPClient in a separate thread
     * @param message Message to send upon connection to the server
     */
    @Override
    protected TCPClient doInBackground(String... message) {

        //Create a TCPClient (the actual socket builder)
        mTcpClient = new TCPClient(context, Constants.SERVER_IP, Constants.SERVER_PORT,
            new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method calls the onProgressUpdate
                    publishProgress(message);
                }
            }
        );
        mTcpClient.run();
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);

        // We got a full message from the server!
        // Add output from the server to our log
        Log.i(Constants.DEBUG_TAG,"server send: " + values[0]);
        mTcpClient.stopRun();

        //send message to MainActivity by receiver
        Intent intent = new Intent("ServiceAndMainActivityUpdates");
        intent.putExtra("inputExtra", Constants.TOBROADCAST_FROM_SERVER + values[0]);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
}
