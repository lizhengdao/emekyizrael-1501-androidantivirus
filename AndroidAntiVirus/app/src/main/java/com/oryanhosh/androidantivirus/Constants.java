package com.oryanhosh.androidantivirus;

import android.app.AlertDialog;

import java.util.ArrayList;

class Constants {
    public static final String SERVER_IP = "192.168.137.1"; //hotspot IP
    public static final int SERVER_PORT = 9999;
    public static final String DEBUG_TAG = "debugMsg";
    public static final char[] KEYSTORE_PASS = "thanksmiles".toCharArray(); //keystore password for SSL

    public static ArrayList<String> topApps = new ArrayList<>();
    public static AlertDialog alert = null;

    //message types for service
    public static final String TYPE_INIT_MESSAGE            = "initmessage:";
    public static final String TYPE_SEND_MESSAGE_TO_SERVER  = "msgtoserver:";
    public static final String TYPE_EXEC_APPPROCESS         = "execappproc:";
    public static final String TYPE_CHECK_PACKAGE           = "chackpackage:";

    //message types for ServiceAndMainActivityUpdates receiver
    public static final String TOBROADCAST_FROM_SERVER      = "FROMSERVER:";
    public static final String TOBROADCAST_FROM_CACHE       = "FROMCACHE:";
    public static final String TOBROADCAST_FROM_DUMP        = "FROMDUMP:";

    //message types for history and popup messages
    public static final int TYPE_APP        = 0;
    public static final int TYPE_APPS       = 1;
    public static final int TYPE_DOMAIN     = 2;
    public static final int TYPE_DOMAINS    = 3;
    public static final int TYPE_CAMERA     = 4;
    public static final int TYPE_LIBS       = 5;

    private Constants() {}
}
