package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

class DisableAppAdapter extends ArrayAdapter<String> {
    private Context context;
    private int resource;
    private ArrayList<String> packages;

    public DisableAppAdapter(@NonNull Context context, int resource, @NonNull ArrayList<String> packages) {
        super(context, resource, packages);
        this.context = context;
        this.resource = resource;
        this.packages = packages;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(resource, null);

        TextView textViewAppName = view.findViewById(R.id.textViewDisableItemAppName);
        Button buttonEnableApp = view.findViewById(R.id.buttonDisableItemEnableApp);
        ImageView imageViewAppIcon = view.findViewById(R.id.imageViewDisableItemAppIcon);

        textViewAppName.setText(MainActivity.getAppNameByPackage(getContext(), packages.get(position).trim()));
        try
        {
            Drawable drawable = getContext().getPackageManager().getApplicationIcon(packages.get(position).trim());
            imageViewAppIcon.setImageDrawable(drawable);
        } catch (PackageManager.NameNotFoundException e)
        {
            imageViewAppIcon.setVisibility(View.GONE);
        }

        buttonEnableApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("DisableAppAdapterAndFragment");
                intent.putExtra("packageName", packages.get(position));
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                enableApp(packages.get(position).trim());
                DisabledAppsController.removeDisabledAppsItems(context, packages.get(position));
                Toast.makeText(context, "app enabled", Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

    private void enableApp(String packageName)
    {
        try {
            Process p = Runtime.getRuntime().exec("su");
            DataOutputStream dos = new DataOutputStream(p.getOutputStream());
            dos.writeBytes("pm enable " + packageName + "\n");
            dos.writeBytes("exit\n");
            dos.flush();
            dos.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
