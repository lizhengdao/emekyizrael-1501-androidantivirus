package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

class DisabledAppsController {
    private static final String DISABLED_APPS_DATA = "disabled_apps_data";

    public static void addDisabledAppItem(Context context, String disabledApp)
    {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(DISABLED_APPS_DATA, "");
        DisabledAppsData data;

        if (json != null && !json.isEmpty())
        {
            data = gson.fromJson(json, DisabledAppsData.class);
        }
        else
        {
            data = new DisabledAppsData();
        }

        if(!data.getDisabledApps().contains(disabledApp))
        {
            data.getDisabledApps().add(disabledApp);
            saveDataToSharedPreferences(context, data);
        }
    }

    public static void removeDisabledAppsItems(Context context, String disabledApp)
    {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(DISABLED_APPS_DATA, "");
        DisabledAppsData data;

        if (json != null && !json.isEmpty())
        {
            data = gson.fromJson(json, DisabledAppsData.class);
        }
        else
        {
            data = new DisabledAppsData();
        }

        if(data.getDisabledApps().contains(disabledApp))
        {
            data.getDisabledApps().remove(disabledApp);
            saveDataToSharedPreferences(context, data);
        }
    }

    public static ArrayList<String> getDisabledAppsItems(Context context)
    {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(DISABLED_APPS_DATA, "");

        if(json != null && !json.isEmpty())
        {
            DisabledAppsData data = gson.fromJson(json, DisabledAppsData.class);
            return data.getDisabledApps();
        }
        return new ArrayList<>();
    }

    private static SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(DISABLED_APPS_DATA, Context.MODE_PRIVATE);
    }

    private static void saveDataToSharedPreferences(Context context, DisabledAppsData data)
    {
        Gson gson = new Gson();
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();
        String json = gson.toJson(data);
        prefsEditor.putString(DISABLED_APPS_DATA, json);
        prefsEditor.apply();
    }
}
