package com.oryanhosh.androidantivirus;

import java.util.ArrayList;

class DisabledAppsData {
    private ArrayList<String> disabledApps;

    public DisabledAppsData() {
        disabledApps = new ArrayList<>();
    }

    public DisabledAppsData(ArrayList<String> disabledApps) {
        this.disabledApps = disabledApps;
    }

    public ArrayList<String> getDisabledApps() {
        return disabledApps;
    }

}
