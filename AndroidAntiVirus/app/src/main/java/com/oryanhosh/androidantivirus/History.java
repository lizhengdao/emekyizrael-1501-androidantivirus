package com.oryanhosh.androidantivirus;

class History {
    private long time;
    private int type;
    private String data;

    public History() {
    }

    public History(long time, int type, String data) {
        this.time = time;
        this.type = type;
        this.data = data;
    }

    public long getTime() {
        return time;
    }

    public int getType() {
        return type;
    }

    public String getData() {
        return data;
    }
}
