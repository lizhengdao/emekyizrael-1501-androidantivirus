package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.ArrayList;

class HistoryController {
    private static final String HISTORIES_DATA = "histories_data";

    public static void addHistoryItem(Context context, History historyItem)
    {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(HISTORIES_DATA, "");
        HistoryData data;

        if (json != null && !json.isEmpty())
        {
            data = gson.fromJson(json, HistoryData.class);
        }
        else
        {
            data = new HistoryData();
        }

        data.getHistoryArrayList().add(historyItem);

        saveDataToSharedPreferences(context, data);
    }

    public static void removeAllHistoryItems(Context context)
    {
        saveDataToSharedPreferences(context, new HistoryData());
    }

    public static ArrayList<History> getHistoryItems(Context context)
    {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(HISTORIES_DATA, "");

        if(json != null && !json.isEmpty())
        {
            HistoryData data = gson.fromJson(json, HistoryData.class);
            return data.getHistoryArrayList();
        }
        return new ArrayList<>();
    }

    private static SharedPreferences getSharedPreferences(Context context)
    {
        return context.getSharedPreferences(HISTORIES_DATA, Context.MODE_PRIVATE);
    }

    private static void saveDataToSharedPreferences(Context context, HistoryData data)
    {
        Gson gson = new Gson();
        SharedPreferences.Editor prefsEditor = getSharedPreferences(context).edit();
        String json = gson.toJson(data);
        prefsEditor.putString(HISTORIES_DATA, json);
        prefsEditor.apply();
    }
}
