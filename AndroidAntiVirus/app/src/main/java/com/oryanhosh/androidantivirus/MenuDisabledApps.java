package com.oryanhosh.androidantivirus;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MenuDisabledApps extends Fragment{
    private ListView listViewDisableApps;
    private TextView textViewEmptyDisableApp;
    private ArrayList<String> packages = new ArrayList<>();

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("DISABLED APPS");

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(
                broadcastReceiver, new IntentFilter("DisableAppAdapterAndFragment"));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.menu_disabled_apps, container, false);

        listViewDisableApps = view.findViewById(R.id.listViewDisableApps);
        textViewEmptyDisableApp = view.findViewById(R.id.textViewEmptyDisableApps);

        packages.clear();
        packages= DisabledAppsController.getDisabledAppsItems(getContext());

        if(packages.isEmpty())
        {
            textViewEmptyDisableApp.setVisibility(View.VISIBLE);
            listViewDisableApps.setVisibility(View.INVISIBLE);
        }
        else if(getContext() != null)
        {
            DisableAppAdapter adapter = new DisableAppAdapter(getContext(), R.layout.disable_app_item, packages);
            listViewDisableApps.setAdapter(adapter);
        }

        return view;
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            String packageName = intent.getStringExtra("packageName");
            packages.remove(packageName);
            if(packages.isEmpty())
            {
                textViewEmptyDisableApp.setVisibility(View.VISIBLE);
                listViewDisableApps.setVisibility(View.INVISIBLE);
            }
            else
            {
                DisableAppAdapter adapter = new DisableAppAdapter(context, R.layout.disable_app_item, packages);
                listViewDisableApps.setAdapter(adapter);
            }
        }
    };
}
