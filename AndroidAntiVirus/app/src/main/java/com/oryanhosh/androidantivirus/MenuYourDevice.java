package com.oryanhosh.androidantivirus;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

public class MenuYourDevice extends Fragment {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("YOUR DEVICE");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_about_device, container, false);
        ListView listView = view.findViewById(R.id.listViewAboutDevice);
        AboutDeviceAdapter deviceAdapter = new AboutDeviceAdapter(getContext(), R.layout.detail_item, getAppData());
        listView.setAdapter(deviceAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                TextView textViewContent = view.findViewById(R.id.textViewDetailContent);
                TextView textViewTitle= view.findViewById(R.id.textViewDetailTitle);
                ClipboardManager clipboardManager = (ClipboardManager) Objects.requireNonNull(getContext()).getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText(textViewTitle.getText().toString(),textViewContent.getText().toString());
                clipboardManager.setPrimaryClip(clipData);
                Toast.makeText(getContext(), "copy to clipboard", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        return view;
    }


    private String[] getAppData(){
        String osVersion = System.getProperty("os.version");
        String apiLevel = String.valueOf(Build.VERSION.SDK_INT);
        String device = android.os.Build.DEVICE;
        String model = android.os.Build.MODEL;
        String product = android.os.Build.PRODUCT;
        return new String[]{
                "osVersion:" + osVersion,
                "apiLevel:" + apiLevel,
                "device:" + device,
                "model:" + model,
                "product:" + product};
    }
}
