package com.oryanhosh.androidantivirus;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.KeyStore;

import javax.net.ssl.SSLSocket;

public class TCPClient {

    public SSLSocket socket = null;
    private Context context;
    private String serverMessage;
    private static String SERVER_IP;
    private static int SERVER_PORT;
    private OnMessageReceived mMessageListener;
    private boolean mRun = false;

    // These handle the I/O
    private PrintWriter out;
    private BufferedReader in;

    /**
     *  Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(Context context, String ip, int port, OnMessageReceived listener)
    {
        this.context = context;
        SERVER_IP = ip;
        SERVER_PORT = port;
        mMessageListener = listener;
    }

    /**
     * Sends the message entered by client to the server.
     * @param message text entered by client
     */
    public void sendMessage(String message){
        // As of Android 4.0 we have to send to network in another thread...
        TCPMessageSendTask sender = new TCPMessageSendTask(out, message);
        sender.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void stopRun(){
        mRun = false;
    }

    public void run() {
        mRun = true;
        try {
            Log.i(Constants.DEBUG_TAG, "try to connect...");
            //create a socket to make the connection with the server

            KeyStore ks = KeyStore.getInstance("BKS");
            // Load the keystore file
            InputStream keyIn = context.getResources().openRawResource(R.raw.client_finished);
            ks.load(keyIn, Constants.KEYSTORE_PASS);

            // Create a SSLSocketFactory that allows for self signed certs
            SSLSocketFactory socketFactory = new SSLSocketFactory(ks);
            socketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            // Build our socket with the factory and the server info
            socket = (SSLSocket) socketFactory.createSocket(new Socket(SERVER_IP,SERVER_PORT), SERVER_IP, SERVER_PORT, true);
            socket.startHandshake();
            Log.i(Constants.DEBUG_TAG, "SSLSocket created");
            try {
                // Create the message sender
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                Log.i(Constants.DEBUG_TAG, "Message Sent to server");
                // Create the message receiver
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                // Listen for the messages sent by the server, stopClient breaks this loop
                while (mRun) {
                    serverMessage = in.readLine();
                    if (serverMessage != null && mMessageListener != null) {
                        //call the method messageReceived
                        mMessageListener.messageReceived(serverMessage);
                    }
                    serverMessage = null;
                }

            } catch (Exception e) {
                Log.e(Constants.DEBUG_TAG, "Error: ", e);
            } finally {
                // Close the socket after stopClient is called
                stopRun();
                socket.close();
            }
        } catch (Exception e) {
            Log.e(Constants.DEBUG_TAG, "Error: ", e);

        }
    }

    // Declare the interface. The method messageReceived(String message) will must be implemented
    // in the implementing class
    public interface OnMessageReceived {
        void messageReceived(String message);
    }
}