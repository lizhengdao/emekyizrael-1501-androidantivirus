# Android Anti-Malware
#### by Oryan Hoshkover & Ido Shushan

[![API](https://img.shields.io/badge/API-23%2B-green.svg?style=flat)](https://android-arsenal.com/api?level=23)

### :zap: Android Anti-Malware is an free Android application for rooted devices :zap:


# System Requirements
- Android device with root access
- SDK version 23 or above
- Internet access

# Features
- Applications scanner
- Application libraries scanner
- Cache and junk cleaner
- Realtime background camera using scanner
- Realtime domains scanner
- Disable harmul applications

# Setup
Put the following files in the directory /data/local/tmp/
- app-debug.apk in /data/local/tmp
- .sh files

Put the following files in the directory /sdcard/
- plugin[number].apk files

# Special Thanks
- Nir Drang - team leader
- Yaron Nidam - mentor