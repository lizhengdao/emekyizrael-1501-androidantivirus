package com.oryanhosh.cache_cleaner_plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public Main() {
        //System.out.println("call to calculateTotalCache() or deleteAllCache()");
    }

    public void calculateTotalCache()
    {
        Process p = null;
        try {
/*            p = Runtime.getRuntime().exec("chmod 777 /data/local/tmp/calc_cache_files_size.sh");
            p.waitFor();*/
            p = new ProcessBuilder().command("/data/local/tmp/antimalwarefiles/calc_cache_files_size.sh").start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if (p != null) {
            String output = convertStreamToString(p.getInputStream());
            //System.out.println("Total cache: " + output.replace('\n', ' ') + "KB");
            System.out.print(output.replace('\n', ' '));
        } else {
            //System.out.println("ERROR: process is null");
        }
    }

    public void deleteAllCache()
    {
        Process p = null;
        try {
/*            p = Runtime.getRuntime().exec("chmod +x /data/local/tmp/delete_cache_files.sh");
            p.waitFor();*/
            p = new ProcessBuilder().command("/data/local/tmp/antimalwarefiles/delete_cache_files.sh").start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if (p != null) {
            System.out.print("CACHE DELETED SUCCESSFULLY");
        } else {
            //System.out.println("ERROR: process is null");
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
