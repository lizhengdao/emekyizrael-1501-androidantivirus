package com.oryanhosh.network_sniffer_plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {

    public Main() {
        System.out.println("call to sniffDNS()");
    }

    public void sniffDNS()
    {
        Process p = null;
        try {
            System.out.println("1");
            p = Runtime.getRuntime().exec("/data/local/tmp/antimalwarefiles/sniff_dns.sh");
            System.out.println("2");
            p.waitFor();
            System.out.println("3");
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void readDomains()
    {
        Process p = null;
        try {
            p = new ProcessBuilder().command("/data/local/tmp/antimalwarefiles/read_domains.sh").start();
            p.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if(p != null)
        {
            String output = convertStreamToString(p.getInputStream());
            System.out.print(output.replace('\n', ' '));
        }
        else
        {
            //System.out.println("ERROR: process is null");
        }
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
